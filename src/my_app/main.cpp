#include <iostream>
#include <module-1-lib-2.h>
#include <AutoVersion/Application.h>
#include <AutoVersion/Output.h>
#include <QApplication>
#include <QMainWindow>
#include <Qt/AutoVersion/AboutDialog.h>

int main ( int argn, char ** argv )
{
    ::std::cout << "Hello qmake-features!" << ::std::endl;
    foo( "Hello module-1-lib-1 from app!");
    bar( "Hello module-1-lib-2 from app!" );

    QApplication app( argn, argv );
    QMainWindow main_window;
    main_window.show();
    ::AutoVersion::AboutDialog::execute( &main_window );
    return app.exec();
}
